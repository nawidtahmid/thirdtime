// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayerState.h"


int AMyPlayerState::InventoryAccess(int ItemNum, bool isheal)
{
	int last;
	if (isheal == false)
	{
		if (ItemNum >= 3)
		{
			ItemNum = ItemNum - 3;
		}
		last = GunInventory[ItemNum];
	}
	else
	{
		last = HealInventory[0];
	}
	return last;
}

int AMyPlayerState::AssetAccess(int gunCount, bool isheal)
{
	int real;
	if (isheal == false)
	{
		real = GunAssets[gunCount];
	}
	else
	{
		real = HealAssets[gunCount];
	}
	return real;
}

void AMyPlayerState::SetupInventories()
{
	int totalgun = 0;

	for (int i = 0; i < 21; i++)
	{
		if (GunAssets[i] < 100)
		{
			if (totalgun <= 2)
			{
				GunInventory[totalgun] = GunAssets[i];
				totalgun += 1;
			}
			else

			{
				break;
			}
		}
	}
	for (int i = 0; i < 8; i++)
	{
		if (HealAssets[i] < 100)
		{
			HealInventory[0] = HealAssets[i];
			break;
		}
	}
}

int AMyPlayerState::FindEmptySpot(bool isheal)
{
	int spot;
	if (isheal == true)
	{
		for (int i = 0; i < 7; i++)
		{
			if (HealAssets[i] >= 100)
			{
				spot = i;
				break;
			}
		}
	}
	else
	{
		for (int i = 0; i < 21; i++)
		{
			if (GunAssets[i] >= 100)
			{
				spot = i;
				break;
			}
		}
	}

	return spot;
}

void AMyPlayerState::BuyHeal(int heal)
{
	HealAssets[FindEmptySpot(true)] = heal;
}

void AMyPlayerState::SwapHeal(int heal, int slot)
{
	int oldheal = HealAssets[slot];
	for (int i = 0; i < 8; i++)
	{
		if (HealAssets[i] == heal)
		{
			HealAssets[slot] = heal;
			HealAssets[i] = oldheal;
			break;
		}
	}
}

void AMyPlayerState::BuyGun(int gun)
{
	GunAssets[FindEmptySpot(false)] = gun;
}

void AMyPlayerState::SwapGun(int gun, int slot)
{
	int oldgun = GunAssets[slot];
	for (int i = 0; i < 21; i++)
	{
		if (GunAssets[i] == gun)
		{
			GunAssets[slot] = gun;
			GunAssets[i] = oldgun;
			break;
		}
	}
}

void AMyPlayerState::SetIsPurchase(int gunCount, bool changeTo, bool isheal)
{
	if (isheal == false)
	{
		IsPurchasedGun[gunCount] = changeTo;
	}
	else
	{
		IsPurchasedHeal[gunCount] = changeTo;
	}
}

void AMyPlayerState::SetTotalAmmo(int gunCount, int totalammo)
{
	TotalAmmoArr[gunCount] = totalammo;
}

bool AMyPlayerState::UpdateIsPurchased(int guncount, bool isheal)
{
	if (isheal == true)
		return IsPurchasedHeal[guncount];
	else
		return IsPurchasedGun[guncount];
}

int AMyPlayerState::G_UpdateMagSize(int gunCount)
{
	return MagSize[gunCount];
}

float AMyPlayerState::G_UpdateDamage(int gunCount)
{
	return Damage[gunCount];
}

float AMyPlayerState::G_UpdateReloadTime(int gunCount)
{
	return ReloadTime[gunCount];
}

float AMyPlayerState::G_UpdateFireRate(int gunCount)
{
	return FireRate[gunCount];
}

FString AMyPlayerState::G_UpdateGunName(int gunCount)
{
	return GunName[gunCount];
}

FString AMyPlayerState::G_UpdateGunType(int gunCount)
{
	return GunType[gunCount];
}

FString AMyPlayerState::G_UpdateReloadType(int gunCount)
{
	return ReloadType[gunCount];
}

int AMyPlayerState::G_UpdateCreditCost(int gunCount)
{
	return CreditCost_Gun[gunCount];
}

int AMyPlayerState::G_UpdateTotalAmmo(int gunCount)
{
	return TotalAmmoArr[gunCount];
}

int AMyPlayerState::G_UpdateWalkSpeeds(int gunCount)
{
	return WalkSpeed[gunCount];
}

int AMyPlayerState::G_UpdateAmmo(int gunCount)
{
	return Ammo[gunCount];
}

void AMyPlayerState::SetAmmo(int gunCount, int ammo)
{
	Ammo[gunCount] = ammo;
}

FString AMyPlayerState::H_UpdateHealName(int gunCount)
{
	return HealName[gunCount];
}

int AMyPlayerState::H_UpdateCreditCost(int gunCount)
{
	return CostCredits_Heal[gunCount];
}

int AMyPlayerState::H_UpdateRegenerationAmount(int gunCount)
{
	return RegenerationAmount[gunCount];
}

bool AMyPlayerState::H_UpdateIsHealth(int gunCount)
{
	return IsHealth[gunCount];
}

int AMyPlayerState::H_UpdateQuantity(int gunCount)
{
	return HealQuantity[gunCount];
}

void AMyPlayerState::AddQuantity(int gunCount)
{
	int q = HealQuantity[gunCount];
	q += 1;
	HealQuantity[gunCount] = q;
}

void AMyPlayerState::DecQuantity(int gunCount)
{
	int q = HealQuantity[gunCount];
	q -= 1;
	HealQuantity[gunCount] = q;
}

