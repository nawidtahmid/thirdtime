// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "MyPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class POW_API AMyPlayerState : public APlayerState
{
	GENERATED_BODY()

		int GunInventory[3] = { 100, 100, 100 };
	int HealInventory[1] = { 100 };
	int GunAssets[20] = { 100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119 };
	int HealAssets[8] = { 100,101,102,103,104,105,106,107 };

	bool IsPurchasedGun[21] = { false, false,false,false,false,false,false,false,false,false,false, false,false,false,false,false,false,false,false,false,false };
	bool IsPurchasedHeal[8] = { false,false,false,false,false,false,false,false };

	int MagSize[20] = { 7, 8, 8, 5, 64, 20, 25, 30, 72, 12, 4, 6, 5, 2, 10, 30, 45, 10, 20, 25 };
	float Damage[20] = { 25, 20, 8, 25, 7, 6, 8, 10, 2, 15, 45, 30, 35, 40, 25, 12, 15, 18, 20, 28 };
	float ReloadTime[20] = { 2.0f, 1.2f, 1.0f, 1.0f, 2.5f, 1.5f, 2.5f, 5.0f, 3.0f, 3.5f, 4.5f, 3.9f, 5.0f, 5.25f, 4.0f, 0.75f, 0.9f, 1.0f, 0.5f, 1.5f };
	float FireRate[20] = { 2.0f, 1.0f, 0.5f, 1.2f, 0.1f, 0.05f, 0.075f, 0.05f, 0.05f, 3.0f, 3.0f, 2.5f, 5.0f, 4.5f, 1.5f, 0.15f, 0.125f, 0.3f, 0.2f, 0.1 };
	FString GunName[20] = { "Nagant", "Tokarev","Makarov","PSM", "Bizon", "KEDR", "Veresk","GenMG", "Drum Gun","Saiga","KS-23","Bekas","Degtyarev", "Lobaev", "Vintorez", "AK-47","AK-74U","Dragunov", "Simonov","ASh" };
	FString GunType[20] = { "Pistol","Pistol", "Pistol", "Pistol", "Machine Gun","Machine Gun", "Machine Gun", "Machine Gun", "Machine Gun", "Shotgun","Shotgun", "Shotgun", "Sniper","Sniper", "Sniper", "Rifle","Rifle", "Rifle", "Rifle", "Rifle", };
	FString ReloadType[20] = { "Manual","Manual","Manual","Manual","Auto","Auto", "Auto", "Auto", "Auto", "Auto", "Single-Shot","Double-Shot","Single-Shot","Single-Shot","Auto","Manual","Auto","Semi-Auto","Auto","Auto" };
	int CreditCost_Gun[20] = { 20,30,40,50,30,40,45,45,35,60,90,110,120,100,95,95,60,70,60,70 };
	int TotalAmmoArr[20] = { 200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200 };
	int WalkSpeed[20] = { 400, 400, 400, 400, 350, 350, 350, 250, 250, 300, 300, 300, 150, 150, 175, 300, 350, 200, 300, 350 };
	int Ammo[20] = { 7, 8, 8, 3, 64, 30, 25, 30, 72, 12, 4, 6, 5, 2, 10, 30, 45, 10, 20, 25 };

	FString HealName[8] = { "BandAid", "Wraps", "Kit", "E-Drink", "BodyPads", "Helmet", "BulletProof", "MetalCasts" };
	int CostCredits_Heal[8] = { 10, 20, 50, 10, 25, 30, 50, 65 };
	int RegenerationAmount[8] = { 5, 15, 35, 5, 15, 20, 25, 40 };
	int HealQuantity[8] = { 0,0,0,0,0,0,0,0 };
	bool IsHealth[8] = { true, true, true, false, false, false, false, false };


	UFUNCTION(BlueprintCallable, Category = "Inventory")
		int InventoryAccess(int ItemNum, bool isheal);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		int AssetAccess(int gunCount, bool isheal);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void SetupInventories();

	int FindEmptySpot(bool isheal);

	UFUNCTION(BlueprintCallable, Category = "Health")
		void BuyHeal(int heal);

	UFUNCTION(BlueprintCallable, Category = "Health")
		void SwapHeal(int heal, int slot);

	UFUNCTION(BlueprintCallable, Category = "RestPeriod")
		void BuyGun(int gun);

	UFUNCTION(BlueprintCallable, Category = "RestPeriod")
		void SwapGun(int gun, int slot);

	UFUNCTION(BlueprintCallable, Category = "Credits")
		void SetIsPurchase(int gunCount, bool changeTo, bool isheal);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		void SetTotalAmmo(int gunCount, int totalammo);

	UFUNCTION(BlueprintCallable, Category = "Credits")
		bool UpdateIsPurchased(int guncount, bool isheal);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		int	G_UpdateMagSize(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		float G_UpdateDamage(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		float G_UpdateReloadTime(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		float G_UpdateFireRate(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		FString G_UpdateGunName(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		FString G_UpdateGunType(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		FString G_UpdateReloadType(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		int G_UpdateCreditCost(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		int G_UpdateTotalAmmo(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		int G_UpdateWalkSpeeds(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		int G_UpdateAmmo(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		void SetAmmo(int gunCount, int ammo);

	UFUNCTION(BlueprintCallable, Category = "Heals")
		FString H_UpdateHealName(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Heals")
		int H_UpdateCreditCost(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Heals")
		int H_UpdateRegenerationAmount(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Heals")
		bool H_UpdateIsHealth(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Heals")
		int H_UpdateQuantity(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Heals")
		void AddQuantity(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Heals")
		void DecQuantity(int gunCount);







};