// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InventoryAI.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class POW_API UInventoryAI : public UActorComponent
{
	GENERATED_BODY()

	int GunInventory[3] = { 100, 100, 100 };

	int MagSize[21] = { 7, 8, 8, 6, 3, 64, 30, 25, 30, 72, 12, 4, 6, 5, 2, 10, 30, 45, 10, 20, 25 };
	float Damage[21] = { 14, 12, 10, 8, 30, 3, 8, 12, 10, 2, 15, 45, 30, 35, 40, 25, 12, 15, 18, 20, 28 };
	float ReloadTime[21] = { 2.0f, 1.2f, 1.0f, 1.5f, 1.0f, 2.5f, 2.0f, 2.5f, 3.0f, 3.0f, 3.5f, 4.5f, 3.9f, 5.0f, 5.25f, 4.0f, 0.75f, 0.9f, 1.0f, 0.5f, 1.5f };
	float FireRate[21] = { 1.75f, 1.5f, 1.25f, 1.5f, 1.25f, 0.1f, 0.0625f, 0.1f, 0.05f, 0.05f, 10.0f, 3.0f, 2.5f, 5.0f, 4.5f, 1.5f, 0.15f, 0.125f, 0.3f, 0.2f, 0.1 };
	FString GunName[21] = { "Nagant", "Tokarev","Makarov", "Silenced","PSM", "Bizon", "KEDR", "Veresk","GenMG", "Drum Gun","Saiga","KS-23","Bekas","Degtyarev", "Lobaev", "Vintorez", "AK-47","AK-74U","Dragunov", "Simonov","ASh" };
	int CreditCost_Gun[21] = { 20,30,40,20,50,30,40,45,45,35,60,90,110,120,100,95,95,60,70,60,70 };
	FString GunType[21] = { "Pistol","Pistol", "Pistol", "Pistol", "Pistol", "Machine Gun","Machine Gun", "Machine Gun", "Machine Gun", "Machine Gun", "Shotgun","Shotgun", "Shotgun", "Sniper","Sniper", "Sniper", "Rifle","Rifle", "Rifle", "Rifle", "Rifle", };



	UFUNCTION(BlueprintCallable, Category = "Inventory")
		int InventoryAccess(int ItemNum);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		int	G_UpdateMagSize(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		float G_UpdateDamage(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		float G_UpdateReloadTime(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		float G_UpdateFireRate(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		FString G_UpdateGunName(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		int G_UpdateCreditCost(int gunCount);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		void G_BuyGun(int gun, int loc);

	UFUNCTION(BlueprintCallable, Category = "Guns")
		FString G_UpdateGunType(int gunCount);

public:	
	// Sets default values for this component's properties
	UInventoryAI();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
