// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryAI.h"

// Sets default values for this component's properties
UInventoryAI::UInventoryAI()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInventoryAI::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UInventoryAI::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

int UInventoryAI::InventoryAccess(int ItemNum)
{
	return GunInventory[ItemNum];
}

int UInventoryAI::G_UpdateMagSize(int gunCount)
{
	return MagSize[gunCount];
}

float UInventoryAI::G_UpdateDamage(int gunCount)
{
	return Damage[gunCount];
}

float UInventoryAI::G_UpdateReloadTime(int gunCount)
{
	return ReloadTime[gunCount];
}

float UInventoryAI::G_UpdateFireRate(int gunCount)
{
	return FireRate[gunCount];
}

FString UInventoryAI::G_UpdateGunName(int gunCount)
{
	return GunName[gunCount];
}

int UInventoryAI::G_UpdateCreditCost(int gunCount)
{
	return CreditCost_Gun[gunCount];
}

void UInventoryAI::G_BuyGun(int gun, int loc)
{
	GunInventory[loc] = gun;
}

FString UInventoryAI::G_UpdateGunType(int gunCount)
{
	return GunType[gunCount];
}
